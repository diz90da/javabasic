package com.ljseok.javabasic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaBasicApplication {

    public static void main(String[] args) {SpringApplication.run(JavaBasicApplication.class, args);

        System.out.println();
        boolean mybox1 = true;
        System.out.println(mybox1);
        char mybox2 = 'a';
        System.out.println(mybox2);

        Character myBox22 = 'a';
        String myBox222 = "AAAaAA";
        System.out.println(myBox22);
        System.out.println(myBox222);

        byte    myBox3 = 1;
        Byte    myBox33 = 11;
        System.out.println(myBox3);
        System.out.println(myBox33);

        short myBox4 = 5;
        int myBox5 = 6;
        long myBox6 = 25L;
        System.out.println(myBox4);
        System.out.println(myBox5);
        System.out.println(myBox6);

        float myBox7 = 5.5f;
        double myBox8 = 15.2134;
        System.out.println(myBox7);
        System.out.println(myBox8);




    }




}
